import { NavLink } from "react-router-dom";

const Nav = () => {
  return (
    <nav className="m-2 flex justify-center space-x-4 p-2 text-center">
      <NavLink to="/" className={({isActive}) => isActive? "text-blue-400": ""}>Home</NavLink>
      <NavLink to="/about" className={({isActive}) => isActive? "text-blue-400": ""}>About</NavLink>
    </nav>
  );
};

export default Nav;
