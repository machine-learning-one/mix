import { useState } from "react";

const Root = () => {
  const [count, setCount] = useState(0);

  return (
    <>
      <h1 className="font-bold">Reactive</h1>
      <button onClick={() => setCount((count) => count + 1)}>
        count is {count}
      </button>
    </>
  );
};

export default Root;
