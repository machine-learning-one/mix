const About = () => {
  return (
    <div>
      <h1 className="font-bold">About</h1>
      <p>A starter template for interactivity driven applications</p>
    </div>
  );
};

export default About;
