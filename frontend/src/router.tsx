import { createBrowserRouter } from "react-router-dom";
import Root from "./routes/Root";
import Error from "./routes/Error";
import About from "./routes/About";
import Shell from "./layouts/Shell";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Shell />,
    errorElement: <Error />,
    children: [
      {
        path: "/",
        element: <Root />,
      },
      {
        path: "/about",
        element: <About />,
      },
    ],
  },
]);

export default router;
