// Adapted from: https://github.com/spf13/viper/blob/master/viper.go
package meta

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/spf13/pflag"
	bolt "go.etcd.io/bbolt"
)

// runtime is a prioritized configuration registry inspired by Viper.
// It maintains a set of configuration sources, dynamically fetches
// values at runtime according to the source's priority.
// The scope of this struct is far narrower than Viper's.
// Viper is recommended for more complex use cases.
// The priority of the sources is the following:
// 1. overrides
// 2. flags
// 3. env. variables
// 4. bbolt db
// 5. defaults
type runtime struct {
	overrides      map[string]interface{}
	flags          map[string]*pflag.Flag
	envPrefix      string
	envKeyReplacer *strings.Replacer
	dbPath         string
	dbBucket       string
	defaults       map[string]interface{}
}

func (r *runtime) setEnvPrefix(prefix string) {
	r.envPrefix = prefix
}

func (r *runtime) setEnvKeyReplacer(replacer *strings.Replacer) {
	r.envKeyReplacer = replacer
}

func (r *runtime) setDBPath(path string) {
	r.dbPath = path
}

func (r *runtime) setDBBucket(bucket string) {
	r.dbBucket = bucket
}

func (r *runtime) bindPFlags(flags *pflag.FlagSet) (err error) {
	flags.VisitAll(func(flag *pflag.Flag) {
		err = r.bindPFlag(flag.Name, flag)
		if err != nil {
			return
		}
	})
	return nil
}

func (r *runtime) bindPFlag(key string, flag *pflag.Flag) error {
	if flag == nil {
		return fmt.Errorf("flag for %s is nil", key)
	}
	r.flags[strings.ToLower(key)] = flag
	r.defaults[strings.ToLower(key)] = flag.DefValue
	return nil
}

func (r *runtime) set(key string, value interface{}) {
	r.overrides[key] = value
}

func (r *runtime) lookupOverrides(key string) (interface{}, bool) {
	value, ok := r.overrides[key]
	return value, ok
}

func (r *runtime) lookupFlags(key string) (interface{}, bool) {
	flag, ok := r.flags[key]
	if !ok {
		return nil, false
	}
	if flag.Changed {
		return flag.Value.String(), true
	}
	return nil, false
}

func (r *runtime) lookupEnv(key string) (interface{}, bool) {
	if r.envPrefix != "" {
		key = r.envPrefix + "_" + key
	}
	if r.envKeyReplacer != nil {
		key = r.envKeyReplacer.Replace(key)
	}
	return os.LookupEnv(strings.ToUpper(key))
}

func (r *runtime) lookupDB(key string) (interface{}, bool) {
	db, err := bolt.Open(r.dbPath, 0644, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return nil, false
	}
	defer db.Close()

	value := []byte{}

	err = db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(r.dbBucket))
		if bucket == nil {
			return fmt.Errorf("bucket %q not fount", r.dbBucket)
		}
		value = bucket.Get([]byte(key))
		return nil
	})
	if err != nil || value == nil {
		return nil, false
	}
	return string(value), true
}

func (r *runtime) lookupDefaults(key string) (interface{}, bool) {
	value, ok := r.defaults[key]
	return value, ok
}

func (r *runtime) get(key string) interface{} {
	if value, ok := r.lookupOverrides(key); ok {
		return value
	}
	if value, ok := r.lookupFlags(key); ok {
		return value
	}
	if value, ok := r.lookupEnv(key); ok {
		return value
	}
	if value, ok := r.lookupDB(key); ok {
		fmt.Println("db", key, value, ok)
		return value
	}
	if value, ok := r.lookupDefaults(key); ok {
		return value
	}
	return nil
}

func (r *runtime) write(key string, value interface{}) error {
	db, err := bolt.Open(r.dbPath, 0644, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(r.dbBucket))
		if err != nil {
			return err
		}
		return bucket.Put([]byte(key), []byte(value.(string)))
	})
}

func (r *runtime) remove(key string) error {
	db, err := bolt.Open(r.dbPath, 0644, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(r.dbBucket))
		if bucket == nil {
			return fmt.Errorf("bucket %q not found", r.dbBucket)
		}
		return bucket.Delete([]byte(key))
	})
}

func (r *runtime) save(key string) error {
	value := r.get(key)
	if value == nil {
		return fmt.Errorf("key %q not found", key)
	}
	return r.write(key, value)
}
