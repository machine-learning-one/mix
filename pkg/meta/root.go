package meta

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cast"
	"github.com/spf13/pflag"
)

// Acts as a unified interface for accessing comptime and runtime values.
type config struct {
	comptime
	runtime
}

// Internal function for setting runtime defaults.
func (c *config) loadRuntimeDefaults() error {
	// Set db path to $HOME/.<projectIdentifier>/<packageIdentifier>
	// Set db bucket to runtime
	home, err := os.UserHomeDir()
	if err != nil {
		return err
	}

	path := filepath.Join(home, fmt.Sprintf(".%s", root.projectIdentifier))
	os.MkdirAll(path, 0644)

	path = filepath.Join(path, root.packageIdentifier)
	c.setDBPath(path)
	c.setDBBucket("runtime")

	// Set env prefix to <projectIdentifier>_
	// Set env key replacer to strings.NewReplacer("-", "_")
	c.setEnvPrefix(root.getProjectIdentifier())
	c.setEnvKeyReplacer(strings.NewReplacer("-", "_"))

	return nil
}

// The root instance acts as a package-level facade.
var root = &config{
	runtime: runtime{
		overrides: make(map[string]interface{}),
		flags:     make(map[string]*pflag.Flag),
		defaults:  make(map[string]interface{}),
	},
}

func init() {
	root.loadBuildInfo()
	root.loadRuntimeDefaults()
}

func GetProjectIdentifier() string {
	return root.getProjectIdentifier()
}

func GetPackageIdentifier() string {
	return root.getPackageIdentifier()
}

func GetDependencyVersion(pkg string) string {
	return root.getDependencyVersion(pkg)
}

func SetEnvPrefix(prefix string) {
	root.setEnvPrefix(prefix)
}

func SetEnvKeyReplacer(replacer *strings.Replacer) {
	root.setEnvKeyReplacer(replacer)
}

func SetDBPath(path string) {
	root.setDBPath(path)
}

func SetDBBucket(bucket string) {
	root.setDBBucket(bucket)
}

func BindPFlags(flags *pflag.FlagSet) error {
	return root.bindPFlags(flags)
}

func Set(key string, value interface{}) {
	root.set(key, value)
}

func Get(key string) interface{} {
	return root.get(key)
}

func Write(key string, value interface{}) error {
	return root.write(key, value)
}

func Remove(key string) error {
	return root.remove(key)
}

func Save(key string) error {
	return root.save(key)
}

func GetBool(key string) bool {
	return cast.ToBool(root.get(key))
}

func GetString(key string) string {
	return cast.ToString(root.get(key))
}

func GetInt(key string) int {
	return cast.ToInt(root.get(key))
}

func GetUint(key string) uint {
	return cast.ToUint(root.get(key))
}
