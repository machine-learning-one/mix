package meta

import (
	"runtime/debug"
	"strings"
)

// comptime is used to store build information and avoid dynamic lookups.
type comptime struct {
	projectIdentifier string
	packageIdentifier string
	dependencies      map[string]string
}

// Internal function for setting build metadata.
func (c *comptime) loadBuildInfo(packages ...string) {
	bi, ok := debug.ReadBuildInfo()
	if !ok {
		return
	}

	projectSegments := strings.Split(bi.Main.Path, "/")
	c.projectIdentifier = projectSegments[len(projectSegments)-1]

	packageSegments := strings.Split(bi.Path, "/")
	c.packageIdentifier = packageSegments[len(packageSegments)-1]

	vendoredVersions := make(map[string]string)
	for _, dep := range bi.Deps {
		for _, pkg := range packages {
			if dep.Path == pkg {
				vendoredVersions[pkg] = dep.Version
			}
		}
	}
	c.dependencies = vendoredVersions
}

func (c *comptime) getProjectIdentifier() string {
	return c.projectIdentifier
}

func (c *comptime) getPackageIdentifier() string {
	return c.packageIdentifier
}

func (c *comptime) getDependencyVersion(pkg string) string {
	return c.dependencies[pkg]
}
