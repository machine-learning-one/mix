package logger

import (
	"context"
	"os"

	"github.com/rs/zerolog"
)

// Creates a new logger with the given level.
func New(level string) zerolog.Logger {
	logger := zerolog.
		New(os.Stderr).
		With().
		Timestamp().
		Logger()

	lvl, err := zerolog.ParseLevel(level)
	if err != nil {
		logger.Fatal().Msgf("failed to parse log level, provided: %s, expected one of: trace, debug, info, warn", level)
	}
	return logger.Level(lvl)
}

// Returns the associated logger for the given context.
func Get(ctx context.Context) *zerolog.Logger {
	return zerolog.Ctx(ctx)
}
