// Adapted from: https://github.com/moby/buildkit/blob/master/util/appcontext/appcontext.go
package context

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

var contextCache context.Context
var syncOnce sync.Once

// Context returns a context.Context that is canceled when the process receives
// an os-specific termination signal. The result is cached and reused on subsequent calls.
// Suitable for use in main() as the top-level context
func Context() context.Context {
	syncOnce.Do(func() {
		signals := make(chan os.Signal)
		terminationSignals := []os.Signal{os.Interrupt, syscall.SIGTERM}
		signal.Notify(signals, terminationSignals...)

		const exitLimit = 2
		retries := 0

		ctx := context.Background()

		ctx, cancel := context.WithCancel(ctx)
		contextCache = ctx

		go func() {
			for {
				<-signals
				cancel()
				retries++
				if retries >= exitLimit {
					os.Exit(1)
				}
			}
		}()
	})
	return contextCache
}
