// Adapted from: https://github.com/golang/go/blob/master/src/context/context.go
package context

import (
	"context"
	"time"
)

// An internal type implementing context.Context.
type emptyCtx int

func (*emptyCtx) Deadline() (deadline time.Time, ok bool) { return }
func (*emptyCtx) Done() <-chan struct{}                   { return nil }
func (*emptyCtx) Err() error                              { return nil }
func (*emptyCtx) Value(key any) any                       { return nil }

var empty = new(emptyCtx)

// Empty returns a non-nil, empty Context. It is never canceled, has no values, and has no deadline.
// It acts as a placeholder when a proper Context is unavailable.
func Empty() context.Context {
	return empty
}
