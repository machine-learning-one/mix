package reset

import (
	"github.com/spf13/cobra"
	"go.machinelearning.one/mix/pkg/logger"
	"go.machinelearning.one/mix/pkg/meta"
)

var portCmd = &cobra.Command{
	Use:   "port",
	Short: "Reset the port to default",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		// Get the context from the root command.
		ctx := cmd.Context()

		// Instantiate a logger and attach it to the context.
		lg := logger.New(meta.GetString("log-level"))
		ctx = lg.WithContext(ctx)

		// Remove the port from the db.
		err := meta.Remove("port")
		if err != nil {
			lg.Fatal().Err(err).Msg("failed to reset port")
		}
	},
}
