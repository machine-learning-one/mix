package reset

import "github.com/spf13/cobra"

var RootCmd = &cobra.Command{
	Use:   "reset",
	Short: "Reset a configuration value to default",
}

func init() {
	RootCmd.AddCommand(portCmd)
}
