package set

import "github.com/spf13/cobra"

var RootCmd = &cobra.Command{
	Use:   "set",
	Short: "Set a configuration value",
}

func init() {
	RootCmd.AddCommand(portCmd)
}
