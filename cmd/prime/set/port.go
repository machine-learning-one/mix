package set

import (
	"strconv"

	"github.com/spf13/cobra"
	"go.machinelearning.one/mix/pkg/logger"
	"go.machinelearning.one/mix/pkg/meta"
)

var portCmd = &cobra.Command{
	Use:   "port",
	Short: "Set the port to listen on",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// Get the context from the root command.
		ctx := cmd.Context()

		// Instantiate a logger and attach it to the context.
		lg := logger.New(meta.GetString("log-level"))
		ctx = lg.WithContext(ctx)

		// Parse the port from the first argument.
		_, err := strconv.Atoi(args[0])
		if err != nil {
			lg.Fatal().Msgf("failed to parse port %q", args[0])
		}

		// Set the port as the default in the meta database.
		err = meta.Write("port", args[0])
		if err != nil {
			lg.Fatal().Err(err).Msg("failed to set port as default")
		}
	},
}
