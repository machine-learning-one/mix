package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"go.machinelearning.one/mix/cmd/prime/reset"
	"go.machinelearning.one/mix/cmd/prime/set"
	"go.machinelearning.one/mix/pkg/context"
	"go.machinelearning.one/mix/pkg/logger"
	"go.machinelearning.one/mix/pkg/meta"
)

var rootCmd = &cobra.Command{
	Use:   meta.GetPackageIdentifier(),
	Short: "A scaffold for enriching docker applications with UI",
}

func init() {
	// Disable the completion subcommand that cobra generates by default.
	rootCmd.CompletionOptions.DisableDefaultCmd = true

	// Application level persistent flags to be inherited by all subcommands.
	rootCmd.PersistentFlags().StringP("log-level", "l", "info", "Log Level")
	rootCmd.PersistentFlags().StringP("config", "c", "", fmt.Sprintf("Configuration File (default \"${Home}/.%s/%s\")",
		meta.GetProjectIdentifier(), meta.GetPackageIdentifier()))

	// Bind the flags for prioritized lookup throughout the application.
	if err := meta.BindPFlags(rootCmd.PersistentFlags()); err != nil {
		exit(err, "failed to bind flags")
	}

	// Add the subcommands from dedicated packages.
	rootCmd.AddCommand(set.RootCmd)
	rootCmd.AddCommand(reset.RootCmd)

	// Set functions to be run when each command execution begins.
	cobra.OnInitialize(setConfig)
}

// Update the db path to the config file if it is set.
func setConfig() {
	config := meta.GetString("config")
	if config != "" {
		dir := filepath.Dir(config)
		os.MkdirAll(dir, 0644)
		meta.SetDBPath(config)
	}
}

// Helper function for root level errors when logger isn't available through context.
func exit(err error, msg string) {
	lg := logger.New("fatal")
	lg.Fatal().Err(err).Msg(msg)
}

func main() {
	// Create an application level context and set it on the root command.
	ctx := context.Context()
	rootCmd.SetContext(ctx)

	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
