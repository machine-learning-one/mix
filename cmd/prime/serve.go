package main

import (
	"github.com/spf13/cobra"
	"go.machinelearning.one/mix/core/server"
	"go.machinelearning.one/mix/pkg/logger"
	"go.machinelearning.one/mix/pkg/meta"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "start the UI server",
	Run: func(cmd *cobra.Command, args []string) {
		// Get the context from the root command.
		ctx := cmd.Context()

		// Instantiate a logger and attach it to the context.
		lg := logger.New(meta.GetString("log-level"))
		ctx = lg.WithContext(ctx)

		// Configure the server based on parsed parameters and run it.
		port := meta.GetUint("port")
		server.Run(ctx, port)
	},
}

func init() {
	// Serve specific flags.
	serveCmd.Flags().UintP("port", "p", 8080, "Port to display on")

	// Bind the flags for ease of lookup.
	if err := meta.BindPFlags(serveCmd.Flags()); err != nil {
		exit(err, "failed to bind flags")
	}

	// Add the serve command as a subcommand of the root.
	rootCmd.AddCommand(serveCmd)
}
