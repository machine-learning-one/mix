package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/spf13/cobra"
	cctx "go.machinelearning.one/mix/pkg/context"
	"go.machinelearning.one/mix/pkg/logger"
	"go.machinelearning.one/mix/pkg/meta"
)

type InstructionSet struct {
	Primary   string   `json:"primary"`
	Secondary []string `json:"secondary"`
}

var rootCmd = &cobra.Command{
	Use:   meta.GetPackageIdentifier(),
	Short: "Concurrently execute commands",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// Get the context from the root command.
		ctx := cmd.Context()

		// Instantiate a logger and attach it to the context.
		lg := logger.New(meta.GetString("log-level"))
		ctx = lg.WithContext(ctx)
		ctx, cancel := context.WithCancel(ctx)

		// Get the file from the context.
		f := args[0]

		// Read the file as json
		raw, err := ioutil.ReadFile(f)
		if err != nil {
			lg.Fatal().Msgf("failed to read the file: %s", f)
		}
		// Unmarshal the json into a struct
		var instructions InstructionSet
		if err := json.Unmarshal(raw, &instructions); err != nil {
			lg.Fatal().Msgf("failed to parse the file as json: %s", f)
		}

		// Iterate over secondary commands and execute them concurrently.
		for _, instruction := range instructions.Secondary {
			go func(instruction string) {
				segments := strings.Split(instruction, " ")
				err := exec.CommandContext(ctx, segments[0], segments[1:]...).Run()
				if err != nil {
					lg := logger.Get(ctx)
					lg.Warn().Err(err).Msg("secondary command failed")
				}
			}(instruction)
		}

		// Create error channel for primary command.
		errPrim := make(chan error, 1)

		// Create a done channel for the primary command.
		done := make(chan struct{})

		// Execute the primary command.
		go func() {
			segments := strings.Split(instructions.Primary, " ")
			cmdPrime := exec.CommandContext(ctx, segments[0], segments[1:]...)
			cmdPrime.Stdout = os.Stdout
			cmdPrime.Stderr = os.Stderr
			err := cmdPrime.Run()
			if err != nil {
				errPrim <- err
			} else {
				done <- struct{}{}
			}
		}()

		gracePeriod := 1 * time.Second

		cleanUp := func() {
			close(errPrim)
			close(done)
			time.Sleep(gracePeriod)
		}

		for {
			select {
			case err := <-errPrim:
				// If the primary command fails, cancel the context to stop the secondary commands
				// log the error and exit the application.
				cancel()
				cleanUp()
				lg.Fatal().Err(err).Msg("primary command failed")
			case <-done:
				// If the primary command is done, cancel the context to stop the secondary commands
				// and exit the application with success.
				cancel()
				cleanUp()
				os.Exit(0)
			case <-ctx.Done():
				// If the context is cancelled, exit the application.
				cleanUp()
				os.Exit(0)
			}
		}
	},
}

func init() {
	// Disable the completion subcommand that cobra generates by default.
	rootCmd.CompletionOptions.DisableDefaultCmd = true
}

func main() {
	// Create an application level context and set it on the root command.
	ctx := cctx.Context()
	rootCmd.SetContext(ctx)

	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
