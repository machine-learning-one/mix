PROJECT := mix

BOLD := \033[1m
RESET := \033[0m

.DEFAULT_GOAL := help

.PHONY: frontend/build # Build the frontend
frontend/build:
	@echo "${BOLD}Building ${PROJECT}/frontend...${RESET}"
	@cd frontend && pnpm build

.PHONY: prime/build # Build the prime binary
prime/build:
	@echo "${BOLD}Building ${PROJECT}/prime...${RESET}"
	@go build -o bin/prime -ldflags '-s -w' ./cmd/prime

.PHONY: prime/compress # Compress the prime binary
prime/compress:
	@echo "${BOLD}Compressing ${PROJECT}/prime...${RESET}"
	@upx -9 bin/prime

.PHONY: prime/root # Run the root command of prime binary
prime/root:
	@echo "${BOLD}Running prime - root command...${RESET}"
	@cd playground && ../bin/prime

.PHONY: prime/serve # Run the serve command of prime binary
prime/serve:
	@echo "${BOLD}Running prime - serve command...${RESET}"
	@cd playground && ../bin/prime serve

.PHONY: concurrently/build # Build the concurrently binary
concurrently/build:
	@echo "${BOLD}Building ${PROJECT}/concurrently...${RESET}"
	@go build -o bin/concurrently -ldflags '-s -w' ./cmd/concurrently

.PHONY: concurrently/compress # Compress the prime binary
concurrently/compress:
	@echo "${BOLD}Compressing ${PROJECT}/concurrently...${RESET}"
	@upx -9 bin/concurrently

.PHONY: concurrently/root # Run the root command of concurrently binary
concurrently/root:
	@echo "${BOLD}Running concurrently - root command...${RESET}"
	@cd playground && ../bin/concurrently cmd.json

.PHONY: +setup # Setup the project for testing
+setup:
	@echo "$(BOLD)Fetching dependencies...$(RESET)"
	@go mod tidy
	@cd frontend && pnpm i
	@echo "$(BOLD)Setting up the playground directory...$(RESET)"
	@mkdir -p playground

.PHONY: +build # Build a production-ready binaries
+build: frontend/build +refresh prime/build prime/compress concurrently/build concurrently/compress

.PHONY: +run # Runs concurrently with prime as as subprocess
+run:
	@echo "${BOLD}Running ${PROJECT}...${RESET}"
	@cd playground && ../bin/concurrently cmd.json

.PHONY: help # Display the help message
help:
	@echo "${BOLD}Available targets:${RESET}"
	@cat Makefile | grep '.PHONY: [a-z\+]' | sed 's/.PHONY: / /g' | sed 's/ #* / - /g'